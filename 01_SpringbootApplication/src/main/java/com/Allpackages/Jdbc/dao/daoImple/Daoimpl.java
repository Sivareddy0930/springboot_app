package com.Allpackages.Jdbc.dao.daoImple;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import com.Allpackages.Jdbc.dao.*;
@Component
public class Daoimpl implements DaoInterface {
	
	@Autowired
	JdbcTemplate jt;
	
	public void setData() {
		jt.update("insert into Employees values(101,'modi',45000)");
		jt.update("insert into Employees values(102,'amitshaw',40000)");
		jt.update("insert into Employees values(103,'jaishanker',35000)");
		jt.update("insert into Employees values(104,'rahul',30000)");
		
	}
	public List getData() {
		return jt.queryForList("select * from Employees");
	}

}
