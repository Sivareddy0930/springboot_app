package com.Allpackages.Jdbc.runner;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.Allpackages.Jdbc.dao.daoImple.Daoimpl;



@Component
public class Myrunner implements CommandLineRunner{
//	CommandLineRunner is a simple Spring Boot interface with a run method. Spring Boot will automatically call the run method of all beans implementing this interface after the application context has been loaded.
	
	@Autowired
	Daoimpl di;

	@Override
	public void run(String... args) throws Exception {
//		di.setData();
		List l=di.getData();
		l.forEach(empObj->{System.out.println(empObj);});
		
	}

}
