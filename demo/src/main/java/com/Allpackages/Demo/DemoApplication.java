package com.Allpackages.Demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
//Customized scanning to scan two different packages.
@ComponentScan(basePackages= {"com.Allpackages.Demo","com.Secutity.mycar"})
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
	
	@Bean
	public Color myCarColor() {
		
		Color obj=new Color();
		//some logic for car color.
		return obj;
		
	}
	

}
