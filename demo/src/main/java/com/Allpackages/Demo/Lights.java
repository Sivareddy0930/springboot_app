package com.Allpackages.Demo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Lights {

	public Lights() {
		System.out.println("Lights Class Constructor");
	}
	
	
	@Bean
	public Lights carLights() {
		
		Lights obj=new Lights();
		//some logic for car Lights.
		return obj;
		
	}
	

}
