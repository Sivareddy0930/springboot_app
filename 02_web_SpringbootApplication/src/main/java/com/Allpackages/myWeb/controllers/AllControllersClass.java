package com.Allpackages.myWeb.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AllControllersClass {
	@GetMapping("/index")
	public ModelAndView indexPage() {
		return new ModelAndView("index");
	}
	
	@GetMapping("/service")
	public ModelAndView servicePage() {
		return new ModelAndView("service");
	}
	@GetMapping("/orders")
	public ModelAndView ordersPage() {
		return new ModelAndView("orders");
	}
	@GetMapping("/registration")
	public ModelAndView registrationPage() {
		return new ModelAndView("registration");
	}
	

}
